import { createRouter, createWebHistory } from "vue-router";
import ListAllJuz from "@/components/ListAllJuz.vue";

const routes = [
    {
        path: '/',
        name: 'ListAllJuz',
        component: ListAllJuz
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;